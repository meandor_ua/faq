<?php
class Ecomitize_Faq_Adminhtml_Ecomitize_FaqController extends Mage_Adminhtml_Controller_Action
{
    const ID_FIELD = 'id';

    protected $_helper = null;

    /**
     * Retrieve Adminhtml Session
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/ecomitize_faq');
    }

    /**
     * Retrieve helper
     *
     * @return Ecomitize_Faq_Helper_Data
     */
    protected function _getHelper()
    {
        if ($this->_helper === null) {
            $this->_helper = Mage::helper('ecomitize_faq');
        }
        return $this->_helper;
    }

    /**
     * Init page title
     *
     * @return Ecomitize_Faq_Adminhtml_Ecomitize_FaqController
     */
    protected function _initAction()
    {
        $this->_title(Mage::helper('adminhtml')->__('Catalog'))
            ->_title($this->__('FAQ'));

        return $this;
    }

    /**
     * Init question model
     *
     * @return Ecomitize_Faq_Adminhtml_Ecomitize_FaqController
     */
    protected function _initQuestion()
    {
        $questionId = (int) $this->getRequest()->getParam(self::ID_FIELD);
        $question = Mage::getModel('ecomitize_faq/question');

        if ($questionId) {
            $question->load($questionId);
        }
        $this->_setQuestionModel($question);

        return $this;
    }

    /**
     * Question model setter
     *
     * @param Ecomitize_Faq_Model_Question $questionModel
     * @return Ecomitize_Faq_Adminhtml_Ecomitize_FaqController
     */
    protected function _setQuestionModel($questionModel)
    {
        Mage::register('question_data', $questionModel);
        return $this;
    }

    /**
     * Retrieve question model
     *
     * @return Ecomitize_Faq_Model_Question
     */
    protected function _getQuestionModel()
    {
        return Mage::registry('question_data');
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Edit question
     */
    public function editAction()
    {
        $this->_initAction();
        $this->_initQuestion();

        $questionId = $this->_getQuestionModel()->getId();
        $this->_title($questionId ? $this->__('Question #%s', $questionId) : $this->__('New Question'));

        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Save question
     */
    public function saveAction()
    {
        $request = $this->getRequest();

        if (!$request->getPost() || !$request->getParam('faq')) {
            $this->_redirect('*/*/');
            return;
        }

        $questionData = $request->getParam('faq');

        try {
            $this->_initQuestion();
            $question = $this->_getQuestionModel();
            $question->addData($questionData)
                ->save();

            $this->_getSession()->addSuccess($this->_getHelper()->__('The question has been saved.'));
            $this->_redirect('*/*/');
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_getSession()->setQuestionData($questionData);

            $this->_redirect('*/*/edit', array(self::ID_FIELD => $question->getId()));
        }
    }

    /**
     * Delete question
     */
    public function deleteAction()
    {
        $this->_initQuestion();
        $question = $this->_getQuestionModel();

        if ($question->getId()) {
            try {
                $question->delete();
                $this->_getSession()->addSuccess($this->__('The question has been deleted.'));
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array(self::ID_FIELD => $question->getId()));
                return;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Export FAQ grid to CSV format
     */
    public function exportCsvAction()
    {
        $this->loadLayout();
        $fileName   = 'ecomitize_faq.csv';
        $content    = $this->getLayout()->getBlock('ecomitize.faq.grid')->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}
