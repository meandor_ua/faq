<?php
class Ecomitize_Faq_Block_Widget_Faq
    extends Ecomitize_Faq_Block_Faq_Custom
    implements Mage_Widget_Block_Interface
{
    protected $_selectedIds = null;

    public function getFaqItems()
    {
        return $this->getFaqCollection()
            ->addItemIdsFilter($this->getSelectedIds())
            ->getItems();
    }

    public function getSelectedIds()
    {
        if ($this->_selectedIds === null) {
            $ids = $this->getFaqIdsSource() ? $this->getFaqIdsAutomatic() : $this->getFaqIdsManual();
            $ids = preg_split('/\s*\,\s*/', trim($ids), 0, PREG_SPLIT_NO_EMPTY);
            if (empty($ids)) {
                $ids = 0;
            }
            $this->_selectedIds = $ids;
        }

        return $this->_selectedIds;
    }

    public function getCacheKeyInfo()
    {
        $cacheKeyInfo = parent::getCacheKeyInfo();
        return array_merge(
            array(
                'ids_source' => $this->getFaqIdsSource(),
                'ids'        => $this->getSelectedIds(),
            ),
            $cacheKeyInfo
        );
    }
}
