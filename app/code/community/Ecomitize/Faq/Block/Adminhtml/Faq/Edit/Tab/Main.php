<?php
class Ecomitize_Faq_Block_Adminhtml_Faq_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Retrieve Adminhtml Session
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    protected function _prepareForm()
    {
        /** @var Ecomitize_Faq_Helper_Adminhtml $helper */
        $helper = Mage::helper('ecomitize_faq/adminhtml');
        /** @var Ecomitize_Faq_Model_Question $question */
        $question = Mage::registry('question_data');
        $questionId = $question->getId();

        if ($restoredData = $this->_getSession()->getQuestionData()) {
            $question->setData($restoredData);
            $this->_getSession()->unsQuestionData();
        }

        if (!$questionId) {
            $question->setStatus($helper->getDefaultQuestionStatus());
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('question_')
            ->setFieldNameSuffix('faq');

        $fieldset = $form->addFieldset('question_details', array(
            'legend' => $this->__('Question Details'), 'class' => 'fieldset-wide'));

        $fieldset->addField('status', 'select', array(
            'label'     => $this->__('Status'),
            'required'  => true,
            'name'      => 'status',
            'values'    => Mage::getModel('ecomitize_faq/system_config_source_status')->toOptionArray(),
        ));

        $fieldset->addField('question', 'text', array(
            'label'     => $this->__('Question'),
            'required'  => true,
            'name'      => 'question'
        ));

        if ($helper->getIsWysiwygEnabled()) {
            $fieldset->addField('answer','editor', array(
                'name'      => 'answer',
                'label'     => $this->__('Answer'),
                'state'     => 'html',
                'style'     => 'height: 600px;',
                'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            ));
        } else {
            $fieldset->addField('answer', 'textarea', array(
                'name'      => 'answer',
                'label'     => $this->__('Answer'),
                'style'     => 'height:24em;',
            ));
        }

        Mage::dispatchEvent('ecomitize_faq_question_edit_tab_main_prepare_form', array('form' => $form));

        $form->setValues($question->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('General Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('General Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
