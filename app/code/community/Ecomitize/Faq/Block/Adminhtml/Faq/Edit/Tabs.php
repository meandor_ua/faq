<?php
class Ecomitize_Faq_Block_Adminhtml_Faq_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('faq_question_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Question Information'));
    }
}