<?php
class Ecomitize_Faq_Block_Adminhtml_Faq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    const TEXT_FIELD_MAX_SIZE              = 65535;
    const TEXT_FIELD_DEFAULT_TRUNCATE_SIZE = 50;

    protected $_helper = null;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->setId('ecomitize_faqGrid')
            ->setDefaultSort('question_id')
            ->setDefaultDir(Varien_Data_Collection::SORT_ORDER_DESC)
            ->setSaveParametersInSession(true)
            ->setUseAjax(true);
    }

    protected function _getHelper()
    {
        if ($this->_helper === null) {
            $this->_helper = Mage::helper('ecomitize_faq');
        }
        return $this->_helper;
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ecomitize_faq/question')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $defaultTruncate = $this->_isExport ? self::TEXT_FIELD_MAX_SIZE : self::TEXT_FIELD_DEFAULT_TRUNCATE_SIZE;
        $isViewMode = !$this->_isExport;
        $this->addColumn('question_id', array(
            'header'        => $this->__('ID'),
            'align'         => 'right',
            'width'         => '50px',
            'index'         => 'question_id',
        ));

        $this->addColumn('question', array(
            'header'        => $this->__('Question'),
            'align'         => 'left',
            'width'         => '200px',
            'index'         => 'question',
            'type'          => 'text',
            'truncate'      => $defaultTruncate,
            'escape'        => $isViewMode,
        ));

        $this->addColumn('answer', array(
            'header'        => $this->__('Answer'),
            'align'         => 'left',
            'width'         => '200px',
            'index'         => 'answer',
            'type'          => 'text',
            'truncate'      => $defaultTruncate,
            'nl2br'         => $isViewMode,
            'escape'        => $isViewMode,
        ));

        $this->addColumn('status', array(
            'header'        => $this->__('Status'),
            'align'         => 'left',
            'type'          => 'options',
            'options'       => Mage::getModel('ecomitize_faq/system_config_source_status')->toArray(),
            'width'         => '30px',
            'index'         => 'status',
        ));

        $this->addColumn('created_at', array(
            'header'        => $this->__('Created At'),
            'align'         => 'left',
            'type'          => 'datetime',
            'width'         => '50px',
            'index'         => 'created_at',
        ));

        $this->addColumn('updated_at', array(
            'header'        => $this->__('Updated At'),
            'align'         => 'left',
            'type'          => 'datetime',
            'width'         => '50px',
            'index'         => 'updated_at',
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/ecomitize_faq/edit', array('id' => $row->getQuestionId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}
