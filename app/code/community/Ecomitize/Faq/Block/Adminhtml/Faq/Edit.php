<?php
class Ecomitize_Faq_Block_Adminhtml_Faq_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'question_id';
        $this->_blockGroup = 'ecomitize_faq';
        $this->_controller = 'adminhtml_faq';

        $this->_updateButton('save', 'label', $this->__('Save Question'));
        $this->_updateButton('save', 'id', 'save_button');
        $this->_updateButton('delete', 'label', $this->__('Delete Question'));
    }

    public function getHeaderText()
    {
        $question = Mage::registry('question_data');

        if ($question && $question->getId()) {
            return $this->__('Edit Question #%s', $question->getId());
        } else {
            return $this->__('Add New Question');
        }
    }
}
