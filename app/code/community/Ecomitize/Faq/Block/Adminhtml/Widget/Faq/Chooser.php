<?php

class Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser extends Ecomitize_Faq_Block_Adminhtml_Faq_Grid
{
    const IN_ITEMS_FIELD = 'in_items';

    /**
     * Store selected FAQ Ids
     * Used in initial setting selected FAQ items
     *
     * @var array
     */
    protected $_selectedItems = null;

    /**
     * Store hidden item ids field id
     *
     * @var string
     */
    protected $_hiddenElementId = '';

    /**
     * Block construction, prepare grid params
     *
     * @param array $arguments Object data
     */
    public function __construct($arguments = array())
    {
        parent::__construct($arguments);
        $this->setDefaultFilter(array(self::IN_ITEMS_FIELD => 1));

        $uniqId = $this->getRequest()->getParam('uniq_id');
        if ($uniqId) {
            $this->setId($uniqId);
        }
    }

    /**
     * Hidden element id setter
     *
     * @param  string $id
     * @return Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser
     */
    public function setHiddenElementId($id)
    {
        $this->_hiddenElementId = $id;
        return $this;
    }

    /**
     * Hidden element id getter
     *
     * @return string
     */
    public function getHiddenElementId()
    {
        return $this->_hiddenElementId;
    }

    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setHiddenElementId($element->getId() . '_hidden');
        if ($value = $element->getValue()) {
            $this->setSelectedItems($value);
        }

        //Create hidden field that store selected items ids
        /** @var Varien_Data_Form $form */
        $form = $element->getForm();
        $hidden = $form->addField($this->getHiddenElementId(), 'hidden', array(
            'name'  => $element->getName(),
            'value' => $value,
        ));
        $hiddenHtml = $hidden->getElementHtml();
        $form->getElements()->remove($this->getHiddenElementId());

        $element->unsValue()
            ->setValueClass('value2')
            ->setData('after_element_html', $hiddenHtml . $this->toHtml());

        return $element;
    }

    /**
     * Grid row init js callback
     *
     * @return string
     */
    public function getRowInitCallback()
    {
        return '
        function(grid, row){
            if (!grid.seItemsIds) {
                grid.seItemsIds = {};
                if ($(\'' . $this->getHiddenElementId() . '\').value != \'\') {
                    var elementValues = $(\'' . $this->getHiddenElementId() . '\').value.split(\',\');
                    for(var i = 0; i < elementValues.length; i++){
                        grid.seItemsIds[elementValues[i]] = i+1;
                    }
                }
                grid.reloadParams = {};
                grid.reloadParams[\'selected_items[]\'] = Object.keys(grid.seItemsIds);
            }
            var inputs      = Element.select($(row), \'input\');
            if (inputs.length == 0) {
                return;
            }
            var checkbox    = inputs[0];
            var position    = inputs[1];
            var itemsNum  = grid.seItemsIds.length;
            var itemId    = checkbox.value;

            inputs[1].checkboxElement = checkbox;

            var indexOf = Object.keys(grid.seItemsIds).indexOf(itemId);
            if(indexOf >= 0){
                checkbox.checked = true;
                if (!position.value) {
                    position.value = indexOf + 1;
                }
            }

            Event.observe(position,\'change\', function(){
                var checkb = Element.select($(row), \'input\')[0];
                if(checkb.checked){
                    grid.seItemsIds[checkb.value] = this.value;
                    var idsclone = Object.clone(grid.seItemsIds);
                    var bans = Object.keys(grid.seItemsIds);
                    var pos = Object.values(grid.seItemsIds).sort(sortNumeric);
                    var items = [];
                    var k = 0;

                    for(var j = 0; j < pos.length; j++){
                        for(var i = 0; i < bans.length; i++){
                            if(idsclone[bans[i]] == pos[j]){
                                items[k] = bans[i];
                                k++;
                                delete(idsclone[bans[i]]);
                                break;
                            }
                        }
                    }
                    $(\'' . $this->getHiddenElementId() . '\').value = items.join(\',\');
                }
            });
        }
        ';
    }

    /**
     * Grid Row JS Callback
     *
     * @return string
     */
    public function getRowClickCallback()
    {
        return '
            function (grid, event) {
                if(!grid.seItemsIds){
                    grid.seItemsIds = {};
                }

                var trElement   = Event.findElement(event, "tr");
                var isInput     = Event.element(event).tagName == \'INPUT\';
                var inputs      = Element.select(trElement, \'input\');
                var checkbox    = inputs[0];
                var position    = inputs[1].value || 1;
                var checked     = isInput ? checkbox.checked : !checkbox.checked;
                checkbox.checked = checked;
                var itemId    = checkbox.value;

                if(checked){
                    if(Object.keys(grid.seItemsIds).indexOf(itemId) < 0){
                        grid.seItemsIds[itemId] = position;
                    }
                }
                else{
                    delete(grid.seItemsIds[itemId]);
                }

                var idsclone = Object.clone(grid.seItemsIds);
                var bans = Object.keys(grid.seItemsIds);
                var pos = Object.values(grid.seItemsIds).sort(sortNumeric);
                var items = [];
                var k = 0;
                for(var j = 0; j < pos.length; j++){
                    for(var i = 0; i < bans.length; i++){
                        if(idsclone[bans[i]] == pos[j]){
                            items[k] = bans[i];
                            k++;
                            delete(idsclone[bans[i]]);
                            break;
                        }
                    }
                }
                $(\'' . $this->getHiddenElementId() . '\').value = items.join(\',\');
                grid.reloadParams = {};
                grid.reloadParams[\'selected_items[]\'] = items;
            }
        ';
    }

    /**
     * Checkbox Check JS Callback
     *
     * @return string
     */
    public function getCheckboxCheckCallback()
    {
        return 'function (grid, element, checked) {
                    if(!grid.seItemsIds){
                        grid.seItemsIds = {};
                    }
                    var checkbox    = element;

                    checkbox.checked = checked;
                    var itemId    = checkbox.value;
                    if(itemId == \'on\'){
                        return;
                    }
                    var trElement   = element.up(\'tr\');
                    var inputs      = Element.select(trElement, \'input\');
                    var position    = inputs[1].value || 1;

                    if(checked){
                        if(Object.keys(grid.seItemsIds).indexOf(itemId) < 0){
                            grid.seItemsIds[itemId] = position;
                        }
                    }
                    else{
                        delete(grid.seItemsIds[itemId]);
                    }

                    var idsclone = Object.clone(grid.seItemsIds);
                    var bans = Object.keys(grid.seItemsIds);
                    var pos = Object.values(grid.seItemsIds).sort(sortNumeric);
                    var items = [];
                    var k = 0;
                    for(var j = 0; j < pos.length; j++){
                        for(var i = 0; i < bans.length; i++){
                            if(idsclone[bans[i]] == pos[j]){
                                items[k] = bans[i];
                                k++;
                                delete(idsclone[bans[i]]);
                                break;
                            }
                        }
                    }
                    $(\'' . $this->getHiddenElementId() . '\').value = items.join(\',\');
                    grid.reloadParams = {};
                    grid.reloadParams[\'selected_items[]\'] = items;
                }';
    }

    /**
     * Create grid columns
     *
     * @return Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser
     */
    protected function _prepareColumns()
    {
        $this->addColumn(self::IN_ITEMS_FIELD, array(
            'header_css_class' => 'a-center',
            'type'             => 'checkbox',
            'name'             => self::IN_ITEMS_FIELD,
            'values'           => $this->getSelectedItems(),
            'align'            => 'center',
            'index'            => 'question_id',
        ));

        $this->addColumnAfter('position',
            array(
                'header'         => $this->__('Position'),
                'name'           => 'position',
                'type'           => 'number',
                'validate_class' => 'validate-number',
                'index'          => 'position',
                'width'          => '50px',
                'editable'       => true,
                'filter'         => false,
                'edit_only'      => true,
                'sortable'       => false,
            ),
            'updated_at'
        );

        parent::_prepareColumns();
        $this->_exportTypes = array();

        return $this;
    }

    /**
     * Set custom filter for in FAQ item flag
     *
     * @param string $column
     * @return Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == self::IN_ITEMS_FIELD) {
            $itemIds = $this->getSelectedItems();
            if (empty($itemIds)) {
                $itemIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addItemIdsFilter($itemIds);
            } else {
                if ($itemIds) {
                    $this->getCollection()->addItemIdsFilter($itemIds, true);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Disable massaction functioanality
     *
     * @return Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser
     */
    protected function _prepareMassaction()
    {
        return $this;
    }

    /**
     * Adds additional parameter to URL for loading only items grid
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/ecomitize_faq_widget/chooser', array(
            'items_grid'     => true,
            '_current'       => true,
            'uniq_id'        => $this->getId(),
            'selected_items' => implode(',', $this->getSelectedItems())
        ));
    }

    /**
     * Setter
     *
     * @param array $selectedItems
     * @return Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser
     */
    public function setSelectedItems($selectedItems)
    {
        if (is_string($selectedItems)) {
            $selectedItems = explode(',', $selectedItems);
        }
        $this->_selectedItems = $selectedItems;
        return $this;
    }

    /**
     * Set items' positions of saved items
     *
     * @return Ecomitize_Faq_Block_Adminhtml_Widget_Faq_Chooser
     */
    protected function _prepareCollection()
    {
        parent::_prepareCollection();

        $collection = $this->getCollection();
        foreach ($this->getSelectedItems() as $position => $itemId) {
            $faqItem = $collection->getItemById($itemId);
            if ($faqItem) {
                $faqItem->setPosition($position + 1);
            }
        }

        return $this;
    }

    /**
     * Getter
     *
     * @return array
     */
    public function getSelectedItems()
    {
        if ($this->_selectedItems === null) {
            $selectedItems = $this->getRequest()->getParam('selected_items');
            if (!empty($selectedItems)) {
                $this->setSelectedItems($selectedItems);
            }
        }

        return $this->_selectedItems;
    }
}
