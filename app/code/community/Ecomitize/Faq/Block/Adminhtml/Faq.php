<?php
class Ecomitize_Faq_Block_Adminhtml_Faq extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'ecomitize_faq';
        $this->_controller = 'adminhtml_faq';
        $this->_headerText = $this->__('Frequently Asked Questions');

        parent::__construct();
    }
}
