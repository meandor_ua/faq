<?php
class Ecomitize_Faq_Block_Faq_Custom extends Ecomitize_Faq_Block_Faq_Abstract
{
    /**
     * Internal constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setBlockType(self::CUSTOM_BLOCK_TYPE);
    }
}
