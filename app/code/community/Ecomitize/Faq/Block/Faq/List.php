<?php
class Ecomitize_Faq_Block_Faq_List extends Ecomitize_Faq_Block_Faq_Abstract
{
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function addCollectionToPager()
    {
        $pagerBlock = $this->getChild('pager');
        if ($pagerBlock) {
            $pagerBlock->setCollection($this->getFaqCollection());
        }

        return $this;
    }
}
