<?php
class Ecomitize_Faq_Block_Faq_Abstract extends Mage_Core_Block_Template
{
    const RANDOM_BLOCK_TYPE = 'random';
    const CUSTOM_BLOCK_TYPE = 'custom';

    protected $_isActive = null;

    protected $_collection = null;

    protected $_blockType = null;

    protected $_helper = null;

    /**
     * Retrieve helper
     *
     * @return Ecomitize_Faq_Helper_Data
     */
    protected function _getHelper()
    {
        if ($this->_helper === null) {
            $this->_helper = Mage::helper('ecomitize_faq');
        }
        return $this->_helper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getIsActive()) {
            return '';
        }
        return parent::_toHtml();
    }

    public function setBlockType($type = null)
    {
        $this->_blockType = $type;
        return $this;
    }

    public function getBlockType()
    {
        return $this->_blockType;
    }

    public function isBlockEnabled()
    {
        $result = null;

        $blockType = $this->getBlockType();
        if ($blockType) {
            switch ($blockType) {
                case self::RANDOM_BLOCK_TYPE:
                    $result = $this->_getHelper()->getIsRandomBlockEnabled();
                    break;
                case self::CUSTOM_BLOCK_TYPE:
                    $result = $this->_getHelper()->getIsCustomBlockEnabled();
                    break;
            }
        }

        return $result;
    }

    public function getIsActive()
    {
        if ($this->_isActive === null) {
            $this->_isActive = $this->_getHelper()->getIsFaqActive();

            $localSetting = $this->isBlockEnabled();
            if (isset($localSetting)) {
                $this->_isActive = $this->_isActive && $localSetting;
            }
        }

        return $this->_isActive;
    }

    protected function _getCacheLifetime()
    {
        $result = null;

        $blockType = $this->getBlockType();
        if ($blockType) {
            switch ($blockType) {
                case self::RANDOM_BLOCK_TYPE:
                    $result = $this->_getHelper()->getRandomBlockLifetime();
                    break;
                case self::CUSTOM_BLOCK_TYPE:
                    $result = $this->_getHelper()->getCustomBlockLifetime();
                    break;
            }
        }

        return $result;
    }

    /**
     * Retrieve FAQ items collection
     *
     * @return Ecomitize_Faq_Model_Resource_Question_Collection
     */
    public function getFaqCollection()
    {
        if ($this->_collection === null) {
            $this->_collection = Mage::getModel('ecomitize_faq/question')->getCollection()
                ->addActiveStatusFilter();
        }
        return $this->_collection;
    }

    public function getFaqItems()
    {
        return $this->getFaqCollection()->load()->getItems();
    }

    public function getCacheLifetime()
    {
        if (!$this->hasData('cache_lifetime')) {
            $this->setData('cache_lifetime', $this->_getCacheLifetime());
        }

        return $this->getData('cache_lifetime');
    }

    public function getCacheKeyInfo()
    {
        $cacheKeyInfo = parent::getCacheKeyInfo();
        return array_merge(
            array(
                'block_type'    => $this->getBlockType(),
                'is_active'     => $this->getIsActive(),
                'is_cached'     => (bool) $this->getCacheLifetime(),
            ),
            $cacheKeyInfo
        );
    }

    /**
     * Retrieve block cache tags based on product collection
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->getFaqCollection())
        );
    }
}
