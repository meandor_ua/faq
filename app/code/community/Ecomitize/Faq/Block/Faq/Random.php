<?php
class Ecomitize_Faq_Block_Faq_Random extends Ecomitize_Faq_Block_Faq_Abstract
{
    /**
     * Internal constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setBlockType(self::RANDOM_BLOCK_TYPE);
    }

    public function getLimit()
    {
        return $this->_getHelper()->getRandomBlockItemsLimit();
    }

    public function getFaqItems()
    {
        return $this->getFaqCollection()
            ->getRandomItems($this->getLimit())
            ->getItems();
    }

    public function getCacheKeyInfo()
    {
        $cacheKeyInfo = parent::getCacheKeyInfo();
        return array_merge(
            array('limit' => $this->getLimit()),
            $cacheKeyInfo
        );
    }
}
