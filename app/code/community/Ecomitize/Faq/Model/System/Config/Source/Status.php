<?php
class Ecomitize_Faq_Model_System_Config_Source_Status
{
    protected $_helper = null;

    /**
     * Retrieve helper
     *
     * @return Ecomitize_Faq_Helper_Data
     */
    protected function _getHelper()
    {
        if ($this->_helper === null) {
            $this->_helper = Mage::helper('ecomitize_faq');
        }
        return $this->_helper;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        foreach ($this->toArray() as $value => $label) {
            $options[] = array('value' => $value, 'label' => $label);
        }

        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            Ecomitize_Faq_Model_Question::STATUS_ACTIVE      => $this->_getHelper()->__('Active'),
            Ecomitize_Faq_Model_Question::STATUS_INACTIVE    => $this->_getHelper()->__('Inactive'),
        );
    }
}