<?php
class Ecomitize_Faq_Model_Resource_Question
    extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Internal constructor
     */
    protected function _construct()
    {
        $this->_init('ecomitize_faq/question', 'question_id');
    }
}
