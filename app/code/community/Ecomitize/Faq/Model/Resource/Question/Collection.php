<?php
class Ecomitize_Faq_Model_Resource_Question_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    const NUMBER_OF_RETRIES = 5;
    /**
     * Internal constructor
     */
    protected function _construct()
    {
        $this->_init('ecomitize_faq/question');
    }

    protected function _getRandomItemsSelect()
    {
        $adapter = $this->getConnection();

        $masIdsSelect = $adapter->select()->from(
            $this->getMainTable(),
            new Zend_Db_Expr('MAX(' . $adapter->quoteIdentifier('question_id') . ')')
        );

        $randomIdsSelect = $adapter->select()
            ->from('', array('id' => $adapter->quoteInto('CEIL(RAND() * ?)', $masIdsSelect)));

        $mainTableQuestionId = $adapter->quoteIdentifier('main_table.question_id');
        $whereCondition = $mainTableQuestionId . ' >= ' . $adapter->quoteIdentifier('random_ids.id');

        $mainSelect = $this->getSelect()
            ->join(array('random_ids' => $randomIdsSelect), '', array())
            ->where($whereCondition)
            ->order(new Zend_Db_Expr($mainTableQuestionId . ' ' . self::SORT_ORDER_ASC))
            ->limit(1);


        return $mainSelect;

    }

    public function addActiveStatusFilter()
    {
        $this->addFieldToFilter('status', array('eq' => Ecomitize_Faq_Model_Question::STATUS_ACTIVE));
        return $this;
    }

    public function fetchRandomItems($qty = 1)
    {
        $result = array();
        if (empty($qty)) {
            $qty = 1;
        }

        $adapter = $this->getConnection();
        $randomItemsSelect = $this->_getRandomItemsSelect();

        $iterationCount = 0;
        while ((count($result) < $qty) && ($iterationCount < $qty + self::NUMBER_OF_RETRIES)) {
            $iterationCount++;
            $row = $adapter->fetchRow($randomItemsSelect);
            if (empty($row)) {
                break;
            }
            $result[$row['question_id']] = $row;
        }

        return $result;
    }

    public function getRandomItems($qty = 1)
    {
        $this->clear();
        $this->resetData();

        $itemsRecords = $this->fetchRandomItems($qty);
        foreach ($itemsRecords as $record) {
            $item = $this->getNewEmptyItem()->setData($record);
            $this->addItem($item);
        }

        $this->_setIsLoaded(true);

        return $this;
    }

    /**
     * Add filter by FAQ items
     *
     * @param array $itemIds
     * @param bool $exclude
     * @return Ecomitize_Faq_Model_Resource_Question_Collection
     */
    public function addItemIdsFilter($itemIds, $exclude = false)
    {
        $this->addFieldToFilter('question_id', array(($exclude ? 'nin' : 'in') => $itemIds));
        return $this;
    }
}
