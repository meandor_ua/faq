<?php
class Ecomitize_Faq_Model_Question extends Mage_Core_Model_Abstract
{
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;

    protected $_textFields = array('question', 'answer');

    /**
     * Init resource model
     */
    protected function _construct() {
        $this->_init('ecomitize_faq/question');
        parent::_construct();
    }

    protected function _getGmtDate()
    {
        return Mage::getSingleton('core/date')->gmtDate();
    }

    /**
     * Retrieve default list of the text fields of the model
     *
     * @return array
     */
    protected function _getTextFields()
    {
        return $this->_textFields;
    }

    /**
     * Trimming all text fields
     *
     * @return Ecomitize_Faq_Model_Question
     */
    protected function _prepareData()
    {
        foreach ($this->_getTextFields() as $fieldName) {
            if ($fieldContent = $this->getData($fieldName)) {
                $this->setData($fieldName, trim($fieldContent));
            }
        }

        return $this;
    }

    /**
     * Validation of the FAQ item
     *
     * @return Ecomitize_Faq_Model_Question
     * @throws Mage_Core_Exception
     */
    protected function _validate()
    {
        if (!Zend_Validate::is($this->getQuestion(), 'NotEmpty')) {
            $message = Mage::helper('ecomitize_faq')->__('Please enter the question.');
            throw new Mage_Core_Exception($message);
        }

        return $this;
    }

    /**
     * Processing object before save data
     *
     * @return Ecomitize_Faq_Model_Question
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        $this->_prepareData();
        $this->_validate();

        if (!$this->getAnswer()) {
            $this->setInactiveStatus();
        }

        $currentDate = $this->_getGmtDate();
        if (!$this->getId()) {
            $this->setCreatedAt($currentDate);
        }
        $this->setUpdatedAt($currentDate);

        return $this;
    }

    /**
     * Set active status
     *
     * @return Ecomitize_Faq_Model_Question
     */
    public function setActiveStatus()
    {
        return $this->setStatus(self::STATUS_ACTIVE);
    }

    /**
     * Set inactive status
     *
     * @return Ecomitize_Faq_Model_Question
     */
    public function setInactiveStatus()
    {
        return $this->setStatus(self::STATUS_INACTIVE);
    }
}
