<?php
class Ecomitize_Faq_Model_Observer
{
    /**
     * Redirect to 404 page to disable FAQ
     *
     * @return Ecomitize_Faq_Model_Observer
     */
    public function disableControllerAction(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('ecomitize_faq')->getIsFaqActive()) {
            /** @var Mage_Core_Controller_Front_Action $action */
            $action = $observer->getEvent()->getControllerAction();
            $action->norouteAction();
            $action->setFlag('', Mage_Core_Controller_Front_Action::FLAG_NO_DISPATCH, true);
        }

        return $this;
    }
}
