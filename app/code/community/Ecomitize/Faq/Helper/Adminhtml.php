<?php
class Ecomitize_Faq_Helper_Adminhtml extends Mage_Core_Helper_Abstract
{
    const XPATH_FAQ_SETTINGS_DEFAULT_STATUS = 'ecomitize_faq/admin_settings/default_status';

    public function getDefaultQuestionStatus()
    {
        return Mage::getStoreConfig(self::XPATH_FAQ_SETTINGS_DEFAULT_STATUS);
    }

    public function getIsWysiwygEnabled()
    {
        if ($this->isModuleEnabled('Mage_Cms')) {
            return (bool)Mage::getSingleton('cms/wysiwyg_config')->isEnabled();
        }

        return false;
    }
}
