<?php
class Ecomitize_Faq_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XPATH_FAQ_SETTINGS_IS_ACTIVE = 'ecomitize_faq/frontend_settings/is_active';

    const XPATH_FAQ_SETTINGS_RANDOM_BLOCK_ENABLED  = 'ecomitize_faq/frontend_settings/random_block_enabled';
    const XPATH_FAQ_SETTINGS_RANDOM_BLOCK_LIFETIME = 'ecomitize_faq/frontend_settings/random_block_cache_lifetime';
    const XPATH_FAQ_SETTINGS_RANDOM_BLOCK_LIMIT    = 'ecomitize_faq/frontend_settings/random_block_limit';

    const XPATH_FAQ_SETTINGS_CUSTOM_BLOCK_ENABLED  = 'ecomitize_faq/frontend_settings/custom_block_enabled';
    const XPATH_FAQ_SETTINGS_CUSTOM_BLOCK_LIFETIME = 'ecomitize_faq/frontend_settings/custom_block_cache_lifetime';

    public function getIsFaqActive()
    {
        return Mage::getStoreConfigFlag(self::XPATH_FAQ_SETTINGS_IS_ACTIVE, Mage::app()->getStore()->getId());
    }

    public function getIsRandomBlockEnabled()
    {
        return Mage::getStoreConfigFlag(
            self::XPATH_FAQ_SETTINGS_RANDOM_BLOCK_ENABLED,
            Mage::app()->getStore()->getId()
        );
    }

    public function getRandomBlockLifetime()
    {
        return Mage::getStoreConfig(
            self::XPATH_FAQ_SETTINGS_RANDOM_BLOCK_LIFETIME,
            Mage::app()->getStore()->getId()
        );
    }

    public function getRandomBlockItemsLimit()
    {
        return Mage::getStoreConfig(
            self::XPATH_FAQ_SETTINGS_RANDOM_BLOCK_LIMIT,
            Mage::app()->getStore()->getId()
        );
    }

    public function getIsCustomBlockEnabled()
    {
        return Mage::getStoreConfigFlag(
            self::XPATH_FAQ_SETTINGS_CUSTOM_BLOCK_ENABLED,
            Mage::app()->getStore()->getId()
        );
    }

    public function getCustomBlockLifetime()
    {
        return Mage::getStoreConfig(
            self::XPATH_FAQ_SETTINGS_CUSTOM_BLOCK_LIFETIME,
            Mage::app()->getStore()->getId()
        );
    }
}
